## Final Project
Hallo, perkenalkan saya Herdi Herdianurdin dari kelas Laravel SanberCode. Sebelumnya mohon maaf untuk pengerjaan tugas project saya lakukan sendiri.
## Tema Project
Disini saya mengambil tema pilihan dari SanberCode, yaitu forum tanya jawab.
## ERD
[![ERD](https://gitlab.com/herdianurdin/simpleforum/-/raw/master/public/simple-forum.png "ERD")](https://gitlab.com/herdianurdin/simpleforum/-/raw/master/public/simple-forum.png "ERD")
## Setup
```bash
# Install dependency
composer install

# Config .env
cp .env.example .env
php artisan key:generate

# Linking storage folder
mkdir public/storage
php artisan storage:link

# Migration
php artisan migrate:fresh
```
## Link
Video => [Demo-App](https://drive.google.com/file/d/1LuBL-icqHl8CS5j-ZX-VCngpn3LdutyJ/view?usp=sharing "Demo-App")

Sering terjadi 500 error, Web  => [Demo-Deploy](https://simple-forum-final.herokuapp.com/)
