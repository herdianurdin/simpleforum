<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Answer;

class AnswerController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $answers = Answer::where('user_id', Auth::id())
            ->orderBy('created_at', 'desc')
            ->get();

        return view('answer.index', compact('answers'));
    }

    public function edit($id) {
        $answer = Answer::find($id);

        return view('answer.edit', compact('answer'));
    }

    public function storeContent($content) {
        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
        $images = $dom->getElementsByTagName('img');
  
        foreach($images as $k => $img){
            $data = $img->getAttribute('src');

            if (strpos($data, 'data') !== false) {
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);
                $image_name= "/storage/" . time().$k.'.png';
                $path = public_path() . $image_name;
                file_put_contents($path, $data);
                $img->removeAttribute('src');
                $img->setAttribute('src', $image_name);
            }
         }
  
        $content = $dom->saveHTML();

        return $content;
    }

    public function update($id, Request $request) {
        if (Answer::find($id)->user_id !== Auth::id()) {
            return redirect()->back();
        }

        $this->validate($request, [
            'content' => 'required'
        ]);

        $content = $this->storeContent($request['content']);

        Answer::where('id', $id)->update([
            'content' => $content
        ]);

        return redirect('/answers');
    }

    public function destroy($id) {
        $answer = Answer::find($id);

        if ($answer->user_id !== Auth::id()) {
            return redirect()->back();
        }

        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($answer->content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
        $images = $dom->getElementsByTagName('img');

        foreach($images as $k => $img){
            $data = $img->getAttribute('src');
            unlink(public_path() . $data);
        }

        Answer::destroy($id);

        return redirect('/answers');
    }
}
