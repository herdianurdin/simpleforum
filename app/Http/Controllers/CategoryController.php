<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $categories = Category::all();

        return view('category.index', compact('categories'));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required'
        ]);

        Category::create([
            'name' => $request['name']
        ]);

        return redirect('/categories');
    }

    public function update($id, Request $request) {
        $this->validate($request, [
            'name' => 'required'
        ]);

        Category::where('id', $id)->update([
            'name' => $request['name']
        ]);

        return redirect('/categories');
    }

    public function destroy($id) {
        Category::destroy($id);

        return redirect('/categories');
    }
}
