<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Question;
use App\Answer;

class ForumController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except('index', 'discussion');
    }

    public function index() {
        $questions = Question::orderBy('created_at', 'desc')->get();

        return view('forum.index', compact('questions'));
    }

    public function discussion($id) {
        $question = Question::find($id);

        return view('forum.discussion', compact('question'));
    }

    public function storeContent($content) {
        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
        $images = $dom->getElementsByTagName('img');
  
        foreach($images as $k => $img){
            $data = $img->getAttribute('src');

            if (strpos($data, 'data') !== false) {
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);
                $image_name= "/storage/" . time().$k.'.png';
                $path = public_path() . $image_name;
                file_put_contents($path, $data);
                $img->removeAttribute('src');
                $img->setAttribute('src', $image_name);
            }
         }
  
        $content = $dom->saveHTML();

        return $content;
    }

    public function answer($id, Request $request) {
        $this->validate($request, [
            'content' => 'required'
        ]);

        $content = $this->storeContent($request['content']);

        Answer::create([
            'content' => $content,
            'user_id' => Auth::id(),
            'question_id' => $id
        ]);

        return redirect()->back();
   }
}
