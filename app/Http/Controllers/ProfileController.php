<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\User;
use App\Profile;

class ProfileController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('profile.index');
    }

    public function update_user(Request $request) {
        $this->validate($request, [
            'email' => 'required|email|string|max:255'
        ]);

        $user = Auth::user();
        $user->email = $request['email'];
        $user->push();

        if ($request['password']) {
            if (!Hash::check($request['password'], $user->password)) {
                return redirect('/profile')->withInput();
            }

            $this->validate($request, [
                'new-password' => 'required|string|min:8',
                'confirm-new-password' => 'required|string|min:8'
            ]);

            if ($request['new-password'] != $request['confirm-new-password']) {
                return redirect('/profile')->withInput();
            }

            $user->password = Hash::make($request['new-password']);
            $user->push();
        }

        return redirect('/profile');
    }

    public function update_profile(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'age' => 'required|integer',
            'bio' => 'required',
            'address' => 'required'
        ]);

        $user = Auth::user();
        $user->name = $request['name'];
        $user->profile->age = $request['age'];
        $user->profile->bio = $request['bio'];
        $user->profile->address = $request['address'];
        $user->push();

        return redirect('/profile');
    }
}
