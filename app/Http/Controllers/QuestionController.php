<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Category;
use Auth;

class QuestionController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $questions = Question::where('user_id', Auth::id())
            ->orderBy('created_at', 'desc')
            ->get();

        return view('question.index', compact('questions'));
    }

    public function create() {
        $categories = Category::all();

        return view('question.create', compact('categories'));
    }

    public function storeContent($content) {
        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
        $images = $dom->getElementsByTagName('img');
  
        foreach($images as $k => $img){
            $data = $img->getAttribute('src');

            if (strpos($data, 'data') !== false) {
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);
                $image_name= "/storage/" . time().$k.'.png';
                $path = public_path() . $image_name;
                file_put_contents($path, $data);
                $img->removeAttribute('src');
                $img->setAttribute('src', $image_name);
            }
         }
  
        $content = $dom->saveHTML();

        return $content;
    }

    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);

        $content = $this->storeContent($request['content']);

        $question = Question::create([
            'title' => $request['title'],
            'content' => $content,
            'user_id' => Auth::user()->id
        ]);
        $question->categories()->sync($request['category']);

        return redirect('/questions');
    }

    public function view($id) {
        $question = Question::find($id);

        return view('question.view', compact('question'));
    }

    public function edit($id) {
        $question = Question::find($id);

        if ($question->user_id !== Auth::id()) {
            return redirect()->back();
        }

        $categories = Category::all();

        $categories_id = [];
        foreach ($question->categories as $category) {
            $categories_id[] = $category['id'];
        }

        return view('question.edit', compact('question', 'categories', 'categories_id'));
    }

    public function update($id, Request $request) {
        if (Question::find($id)->user_id !== Auth::id()) {
            return redirect()->back();
        }

        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);

        $content = $this->storeContent($request['content']);

        Question::where('id', $id)->update([
            'title' => $request['title'],
            'content' => $content
        ]);
        
        $question = Question::find($id);
        $question->categories()->sync($request['category']);

        return redirect('/questions');
    }

    public function destroy($id) {
        $question = Question::find($id);

        if ($question->user_id !== Auth::id()) {
            return redirect()->back();
        }

        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($question->content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
        $images = $dom->getElementsByTagName('img');

        foreach($images as $k => $img){
            $data = $img->getAttribute('src');
            unlink(public_path() . $data);
        }

        Question::destroy($id);

        return redirect('/questions');
    }
}
