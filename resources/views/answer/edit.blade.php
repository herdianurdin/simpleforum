@extends('layouts.master')
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/plugins/summernote/summernote-bs4.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/plugins/codemirror/lib/codemirror.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/plugins/codemirror/theme/duotone-dark.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/plugins/jquery-selectric/selectric.css') }}">
@endsection
@section('content')
<div class="section-header">
  <div class="section-header-back">
    <a href="/answers" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
  </div>
  <h1>Edit Answer</h1>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active"><a href="/answers">Answers</a></div>
    <div class="breadcrumb-item">Edit Answer</div>
  </div>
</div>
<div class="section-body">
    <div class="card">
        <div class="card-body">
            <h4>{{ $answer->question->title }}</h4>
            {!! $answer->question->content !!}
            <p class="card-text media"><b>Your answer :</b></p>
            <form action="/answers/{{ $answer->id }}/update" method="POST">
              {{ csrf_field() }}
              @method('PUT')
                <div class="form-group my-0">
                    <textarea class="form-control summernote" id="content" name="content" required>{{ $answer->content }}</textarea>
                </div>
                <div class="form-group mb-0 text-right">
                    <button class="btn btn-primary" type="submit"><b>Update Answer</b></button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('script')
<script src="{{ asset('vendor/plugins/summernote/summernote-bs4.js') }}"></script>
<script src="{{ asset('vendor/plugins/codemirror/lib/codemirror.js') }}"></script>
<script src="{{ asset('vendor/plugins/codemirror/mode/javascript/javascript.js') }}"></script>
<script src="{{ asset('vendor/plugins/jquery-selectric/jquery.selectric.min.js') }}"></script>
@endpush