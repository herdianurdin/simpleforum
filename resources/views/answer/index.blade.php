@extends('layouts.master')
@section('content')
<div class="section-header">
  <h1>Answers</h1>
</div>
<div class="section-body">
  <h2 class="section-title">Your Answers :</h2>

  @forelse ($answers as $answer)
    <div class="card">
      <div class="card-body">
        <h4>{{ $answer->question->title }}</h4>
        <div>
          {!! $answer->question->content !!}
        </div>
        <p class="card-text media"><b>Your answer :</b></p>
        <div>
          {!! $answer->content !!}
        </div>
        <div class="d-flex justify-content-end">
          <a class="btn btn-sm btn-dark mr-3" href="/answers/{{ $answer->id }}/edit">Edit</a>
          <form action="/answers/{{ $answer->id }}/delete" method="POST">
            @csrf
            @method('DELETE')
              <button class="btn btn-sm btn-danger" type="submit">Trash</button>
          </form>
        </div>
      </div>
    </div>
  @empty
      <p class="section-lead">You haven't answered the questions in this forum yet! </p>
  @endforelse
</div>
@endsection