@extends('layouts.master')
@section('content')
<div class="section-header">
  <h1>Categories</h1>
</div>
<div class="section-body">
  <div class="card card-primary">
    <div class="card-body">
      <form action="/categories/create" method="POST">
          @csrf
            <div class="form">
                <div class="input-group mb-0">
                    <input type="text" class="form-control" name="name" required placeholder="Category Name">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit"><b>Create</b></button>
                    </div>
                </div>
            </div>
      </form>
    </div>
  </div>
  <div class="card card-primary">
      <div class="card-body">
          @if ($categories->count() == 0)
            <h2 class="section-title mt-0">Categories :</h2>
            <p class="section-lead mb-0">Category is still empty!</p>
          @else
            <table class="table table-bordered mb-0">
                <thead>
                    <tr class="text-center">
                        <th>Name</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categories as $category)
                    <tr>
                        <td>{{ $category->name }}</td>
                        <td>
                            <form action="/categories/{{ $category->id }}/update" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="input-group mb-0">
                                    <input name="name" type="text" value="{{ $category->name }}" class="form-control" placeholder="Category Name" required name="name">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="submit"><b><i class="fa fa-pencil" aria-hidden="true"></i></b></button>
                                    </div>
                                </div>
                            </form>
                        </td>
                        <td class="text-center">
                            <form action="/categories/{{ $category->id }}/delete" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger"><b><i class="fa fa-trash" aria-hidden="true"></i></b></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
          @endif
      </div>
  </div>
</div>
@endsection