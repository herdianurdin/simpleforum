@extends('layouts.master')
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/plugins/summernote/summernote-bs4.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/plugins/codemirror/lib/codemirror.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/plugins/codemirror/theme/duotone-dark.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/plugins/jquery-selectric/selectric.css') }}">
@endsection
@section('content')
<div class="section-header">
  <div class="section-header-back">
    <a href="/" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
  </div>
  <h1>Discussion</h1>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active"><a href="/">Forum</a></div>
    <div class="breadcrumb-item">Discussion</div>
  </div></div>
<div class="section-body">
  <div class="card">
    <div class="card-body">
      <h4>{{ $question->title }}</h4>
      <div>
        {!! $question->content !!}
        <div class="text-small font-weight-bold mb-3">by {{ $question->user->name }}</div>
      </div>
      <div class="badges mb-0">
          @forelse ($question->categories as $category)
              <span class="badge badge-secondary">{{ $category->name }}</span>
          @empty
          @endforelse
      </div>
    </div>
  </div>
  <h2 class="section-title">Answers :</h2>
  <!-- List of Answers -->
  @forelse ($question->answers as $answer)
    <div class="card">
      <div class="card-body media">
        <div class="media-body">
          <div class="media-title">{{ $answer->user->name }}</div>
          <div class="media-description text-muted">
            {!! $answer->content !!}
        </div>
        </div>
      </div>
    </div>
  @empty
    <p class="section-lead">No answer...</p>
  @endforelse
  @guest
    <div class="card card-primary">
      <div class="card-body">
        <h5 class="mb-0">You must <a class="font-weight-bold" href="{{ route('login') }}">Login</a> to be able to discuss...</h5>
      </div>
    </div>
  @else
    <div class="card card-primary" >
      <div class="card-body">
        <form action="/discussion/{{ $question->id }}/answer" method="POST">
          {{ csrf_field() }}
          <div class="section-title mt-0">Your Answer</div>
          <div class="form-group mb-0">
            <textarea class="form-control summernote" id="content" name="content" required></textarea>
          </div>
          <div class="form-group mb-0 text-right">
            <button class="btn btn-primary" type="submit"><b>Submit Answer</b></button>
          </div>
        </form>
      </div>
    </div>
  @endguest
</div>
@endsection
@push('script')
<script src="{{ asset('vendor/plugins/summernote/summernote-bs4.js') }}"></script>
<script src="{{ asset('vendor/plugins/codemirror/lib/codemirror.js') }}"></script>
<script src="{{ asset('vendor/plugins/codemirror/mode/javascript/javascript.js') }}"></script>
<script src="{{ asset('vendor/plugins/jquery-selectric/jquery.selectric.min.js') }}"></script>
@endpush