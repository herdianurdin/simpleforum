@extends('layouts.master')
@section('content')
<div class="section-header">
  <h1>Forum</h1>
</div>
<div class="section-body">
  <h2 class="section-title">Discussion</h2>
  @forelse ($questions as $question)
    <div class="card">
      <div class="card-body">
        <a href="/discussion/{{ $question->id }}"><h4>{{ $question->title }}</h4></a>
        <div>
          {!! $question->content !!}
          <div class="text-small font-weight-bold mb-3">by {{ $question->user->name }}</div>
        </div>
        <div class="badges mb-0">
          @forelse ($question->categories as $category)
              <span class="badge badge-secondary">{{ $category->name }}</span>
          @empty
          @endforelse
        </div>
      </div>
    </div>
  @empty
    <p class="section-lead">No questions to discuss...</p>
  @endforelse
</div>
@endsection