<nav class="navbar navbar-expand-lg main-navbar bg-primary">
    <ul class="navbar-nav mr-auto">
        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
    </ul>
    <ul class="navbar-nav navbar-right">
        @guest
            <li class="nav-item">
                <a class="nav-link nav-link-user" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link nav-link-user" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
        @else
            <li class="nav-item dropdown">
                <a href="#" data-toggle="dropdown" id="navbarDropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">{{ Auth::user()->name }} </a>
                <div class="dropdown-menu dropdown-menu-right py-0">
                    <a href="/profile" class="dropdown-item">Profile</a>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
                </div>
            </li>
        @endguest
    </ul>
</nav>