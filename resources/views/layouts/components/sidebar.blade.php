<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand">
        <a href="/">SIMPLE FORUM</a>
      </div>
      <div class="sidebar-brand sidebar-brand-sm">
        <a href="/">SF</a>
      </div>
      <ul class="sidebar-menu">
        <li class="{{ Request::is('/') || Request::is('discussion*') ? 'active' : ''}}">
            <a class="nav-link" href="/">
                <i class="far fa-newspaper"></i> <span>Forum</span>
            </a>
        </li>
        @if (Auth::user())
        <li class="{{ Request::is('categories*') ? 'active' : ''}}">
            <a class="nav-link" href="/categories">
                <i class="far fa-list-alt"></i> <span>Categories</span>
            </a>
        </li>
        <li class="{{ Request::is('questions*') ? 'active' : ''}}">
            <a class="nav-link" href="/questions">
                <i class="far fa-question-circle"></i> <span>Questions</span>
            </a>
        </li>
        <li class="{{ Request::is('answers*') ? 'active' : ''}}">
            <a class="nav-link" href="/answers">
                <i class="far fa-message"></i> <span>Answers</span>
            </a>
        </li>
        <li class="{{ Request::is('profile') ? 'active' : ''}}">
            <a class="nav-link" href="/profile">
                <i class="far fa-user"></i> <span>Profile</span>
            </a>
        </li>
        @endif
      </ul>
    </aside>
</div>