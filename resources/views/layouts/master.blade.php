<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Simple Forum</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('vendor/plugins/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/plugins/fontawesome/css/all.min.css') }}">

  <!-- CSS Libraries -->
  @yield('css')

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('vendor/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('vendor/css/components.css')}} ">

  <!-- Favicon -->
  <link rel="shortcut icon" href="{{ asset('icon.png') }}">
</head>
<body>
  <header>
    <!-- Navbar Content -->
    @include('layouts.components.navbar')

    <!-- Sidebar Content -->
    @include('layouts.components.sidebar')
  </header>

  <!-- Main Content -->
  <main class="main-content main-wrapper main-wrapper-1">
    <section class="section">
        @yield('content')
    </section>
  </main>
    <!-- General JS Scripts -->
    <script src="{{ asset('vendor/plugins/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/plugins/popper.js') }}"></script>
    <script src="{{ asset('vendor/plugins/tooltip.js') }}"></script>
    <script src="{{ asset('vendor/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendor/plugins/nicescroll/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ asset('vendor/plugins/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/js/stisla.js') }}"></script> 

    <!-- JS Libraies -->
    @stack('script')
    
    <!-- Template JS File -->
    <script src="{{ asset('vendor/js/scripts.js') }}"></script>
    <script src="{{ asset('vendor/js/custom.js') }}"></script>
</body>
</html>