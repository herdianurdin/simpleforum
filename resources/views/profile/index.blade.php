@extends('layouts.master')
@section('content')
<div class="section-header">
  <h1>Profile</h1>
</div>
<div class="section-body">
    <div class="row">
        <div class="col-12 col-sm-12 col-lg-12">
            <div class="card card-primary profile-widget">
                <div class="profile-widget-header">                     
                    <img alt="image" src="{{ Avatar::create(Auth::user()->name)->toBase64() }}" class="rounded-circle profile-widget-picture">
                    <div class="profile-widget-items">
                    <div class="profile-widget-item">
                        <div class="profile-widget-item-label">Questions</div>
                        <div class="profile-widget-item-value">{{ Auth::user()->questions->count() }}</div>
                    </div>
                    <div class="profile-widget-item">
                        <div class="profile-widget-item-label">Answers</div>
                        <div class="profile-widget-item-value">{{ Auth::user()->answers->count() }}</div>
                    </div>
                    </div>
                </div>
                <div class="profile-widget-description py-0">
                    <div class="profile-widget-name mb-0">{{ Auth::user()->name }} {{ Auth::user()->profile->age ?  '(' . Auth::user()->profile->age . ' years old)' : '' }}</div>
                    <div class="profile-widget-name"><small>{{ Auth::user()->email }}</small></div>
                    <p class="mb-0">{{ Auth::user()->profile->bio }}</p>
                    <p><b>{{ Auth::user()->profile->address }}</b></p>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-lg-6">
            <div class="card author-box card-primary">
                <div class="card-body">
                    <h6 class="section-title mt-0 mb-3">Update Account</h6>
                    <form action="/profile/user" method="POST">
                    @csrf
                    @method('PUT')
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input id="email" name="email" type="email" class="form-control" value="{{ Auth::user()->email }}" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Old Password</label>
                            <input id="password" name="password" type="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="new-password">New Password</label>
                            <input id="new-password" name="new-password" type="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="confirm-new-password">Confirm New Password</label>
                            <input id="confirm-new-password" name="confirm-new-password" type="password" class="form-control">
                        </div>
                        <div class="form-group mb-0 text-right">
                            <button class="btn btn-primary" type="submit"><b>Update Account</b></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-lg-6">
            <div class="card author-box card-primary">
                <div class="card-body">
                    <h6 class="section-title mt-0 mb-3">Update Profile</h6>
                    <form action="/profile/profile" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="name">Your Name</label>
                            <input id="name" name="name" type="text" class="form-control" required value="{{ Auth::user()->name }}">
                        </div>
                        <div class="form-group">
                            <label for="age">Age</label>
                            <input id="age" name="age" type="number" min="12" class="form-control" value="{{ Auth::user()->profile->age }}" required>
                        </div>
                        <div class="form-group">
                            <label for="bio">Bio</label>
                            <textarea id="bio" name="bio" class="form-control" required>{{ Auth::user()->profile->bio }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <textarea id="address" name="address" class="form-control" required>{{ Auth::user()->profile->address }}</textarea>
                        </div>
                        <div class="form-group mb-0 text-right">
                            <button class="btn btn-primary" type="submit"><b>Update Profile</b></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection