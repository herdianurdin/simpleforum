@extends('layouts.master')
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/plugins/summernote/summernote-bs4.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/plugins/codemirror/lib/codemirror.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/plugins/codemirror/theme/duotone-dark.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/plugins/jquery-selectric/selectric.css') }}">
@endsection
@section('content')
<div class="section-header">
  <div class="section-header-back">
    <a href="/questions" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
  </div>
  <h1>Create Question</h1>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active"><a href="/questions">Questions</a></div>
    <div class="breadcrumb-item">Create Question</div>
  </div>
</div>
<div class="section-body">
    <div class="card card-primary">
        <form action="/questions/store" method="POST">
            {{ csrf_field() }}
            <div class="card-body">
                <div class="form-group">
                    <div class="section-title mt-0">Title</div>
                    <input id="title" name="title" type="text" class="form-control" required>
                </div>
                <div class="form-group">
                    <div class="section-title mt-0">Content</div>
                    <textarea class="form-control summernote" id="content" name="content" required></textarea>
                </div>
                <div class="form-group">
                    <div class="section-title mt-0">Category</div>
                    @forelse ($categories as $category)
                        <div class="custom-control custom-control-inline custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="{{ $category->id }}" value="{{ $category->id }}" name="category[]">
                            <label class="custom-control-label" for="{{ $category->id }}">{{ $category->name }}</label>
                        </div>
                    @empty
                        <p class="section-lead">No category tags yet...</p>
                    @endforelse
                </div>
                <div class="form-group mb-0 text-right">
                    <button class="btn btn-primary" type="submit"><b>Create Question</b></button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('script')
<script src="{{ asset('vendor/plugins/summernote/summernote-bs4.js') }}"></script>
<script src="{{ asset('vendor/plugins/codemirror/lib/codemirror.js') }}"></script>
<script src="{{ asset('vendor/plugins/codemirror/mode/javascript/javascript.js') }}"></script>
<script src="{{ asset('vendor/plugins/jquery-selectric/jquery.selectric.min.js') }}"></script>
@endpush