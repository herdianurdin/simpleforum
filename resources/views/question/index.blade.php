@extends('layouts.master')
@section('content')
<div class="section-header">
  <h1>Questions</h1>
</div>
<div class="section-body">
  <div class="card card-primary">
    <div class="card-body">
      <a href="/questions/create" class="btn btn-primary"><b>Create Question</b></a>
    </div>
  </div>
  <h2 class="section-title">Your questions :</h2>
  @forelse ($questions as $question)
    <div class="card">
      <div class="card-body">
        <a href="/questions/{{ $question->id }}"><h4>{{ $question['title'] }}</h4></a>
        {!! $question['content'] !!}
        <div class="badges">
          @forelse ($question->categories as $category)
              <span class="badge badge-secondary">{{ $category->name }}</span>
          @empty
          @endforelse
        </div>
        <div class="d-flex justify-content-end">
          <a class="btn btn-sm btn-dark mr-3" href="/questions/{{ $question['id'] }}/edit">Edit</a>
          <form action="/questions/{{ $question['id'] }}/delete" method="POST">
            @csrf
            @method('DELETE')
              <button class="btn btn-sm btn-danger" type="submit">Trash</button>
          </form>
        </div>
      </div>
    </div>      
  @empty
    <p class="section-lead">You have never made a question in this forum!</p>      
  @endforelse

</div>
@endsection