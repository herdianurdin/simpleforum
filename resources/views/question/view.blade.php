@extends('layouts.master')
@section('content')
<div class="section-header">
  <div class="section-header-back">
    <a href="/questions" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
  </div>
  <h1>Question</h1>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active"><a href="/questions">Questions</a></div>
    <div class="breadcrumb-item">Question</div>
  </div></div>
<div class="section-body">
  <div class="card">
    <div class="card-body">
      <h4>{{ $question->title }}</h4>
      <div>
        {!! $question->content !!}
        <div class="text-small font-weight-bold mb-3">by {{ $question->user->name }}</div>
      </div>
      <div class="badges mb-0">
          @forelse ($question->categories as $category)
              <span class="badge badge-secondary">{{ $category->name }}</span>
          @empty
          @endforelse
      </div>
    </div>
  </div>
  <h2 class="section-title">Answers :</h2>
  <!-- List of Answers -->
  @forelse ($question->answers as $answer)
    <div class="card">
      <div class="card-body media">
        <div class="media-body">
          <div class="media-title">{{ $answer->user->name }}</div>
          <div class="media-description text-muted">
            {!! $answer->content !!}
        </div>
        </div>
      </div>
    </div>
  @empty
    <p class="section-lead">No answer...</p>
  @endforelse
</div>
@endsection