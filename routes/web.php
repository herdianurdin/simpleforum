<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'ForumController@index');
Route::get('/discussion/{id}', 'ForumController@discussion');
Route::post('/discussion/{id}/answer', 'ForumController@answer');

Route::get('/categories', 'CategoryController@index');
Route::post('/categories/create', 'CategoryController@store');
Route::put('/categories/{id}/update', 'CategoryController@update');
Route::delete('/categories/{id}/delete', 'CategoryController@destroy');

Route::get('/questions', 'QuestionController@index');
Route::get('/questions/create', 'QuestionController@create');
Route::get('/questions/{id}', 'QuestionController@view');
Route::get('/questions/{id}/edit', 'QuestionController@edit');
Route::post('/questions/store', 'QuestionController@store');
Route::put('/questions/{id}/update', 'QuestionController@update');
Route::delete('/questions/{id}/delete', 'QuestionController@destroy');

Route::get('/answers', 'AnswerController@index');
Route::get('/answers/{id}/edit', 'AnswerController@edit');
Route::put('/answers/{id}/update', 'AnswerController@update');
Route::delete('/answers/{id}/delete', 'AnswerController@destroy');

Route::get('/profile', 'ProfileController@index');
Route::put('/profile/user', 'ProfileController@update_user');
Route::put('/profile/profile', 'ProfileController@update_profile');